[![Author](https://img.shields.io/badge/author-Arlington%20County-lightgrey.svg?colorB=9900cc )](https://www.arlingtonva.us/?utm_source=bitbucket.com&utm_medium=campaign&utm_content=button&utm_campaign=ArlingtonCounty%2Fupprev-extended)
[![Bitbucket Issues](https://img.shields.io/bitbucket/issues-raw/ArlingtonCounty/upprev-extended.svg)](https://bitbucket.org/ArlingtonCounty/upprev-extended/issues)
[![License](https://img.shields.io/badge/license-GPL-yellow.svg)](http://www.gnu.org/licenses/gpl-2.0.html?utm_source=bitbucket.com&utm_medium=campaign&utm_content=button&utm_campaign=ArlingtonCounty%2Fupprev-extended)
[![Twitter](https://img.shields.io/twitter/url/https/bitbucket.com/ArlingtonVA/upprev-extended.svg?style=social)](https://twitter.com/ArlingtonVA)

# upPrev Extended

A WordPress plugin that displays a flyout when scrolling to the bottom of the browser window to encourage the viewer to read the previous post.

Based on original [upPrev](https://wordpress.org/plugins/upprev/) work by Marcin Pietrzak, Jason Pelker and Grzegorz Krzyminski. Extended by [Daniel M. Hendricks](https://www.danhendricks.com/?utm_source=bitbucket.com&utm_medium=referral&utm_content=button&&utm_campaign=ArlingtonCounty%2Fupprev-extended).
